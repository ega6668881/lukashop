import { observer } from 'mobx-react'
import './style.css'
import { useStore } from '../../../../stores/clientStore'
import { useState } from 'react'
import { Link } from 'react-router-dom'

function RegisterForm() {
    const clientStore = useStore().ClientStore 
    const [loginInput, setLoginInput] = useState("")
    const [passwordInput, setPasswordInput] = useState("")
    const [comparePasswordInput, setComparePasswordInput] = useState("")
    const [emailInput, setEmailInput] = useState("")
    const [errorMessage, setErrorMessage] = useState("")
    return (
        <div className='registerForm-wrapper'>
            <div className='registerForm'>
                <h1 className='title'>Регестрация</h1>
                <form onSubmit={(e) => clientStore.registration(e, loginInput, passwordInput, comparePasswordInput, emailInput, setErrorMessage)} className='input-wrapper'>
                    <input className='input' required placeholder='Логин' onChange={(e) => setLoginInput(e.target.value)}></input>
                    <input className='input' required placeholder='Пароль' type="password" onChange={(e) => setPasswordInput(e.target.value)}></input>
                    <input className='input' required placeholder='Повторение пароля' type="password" onChange={(e) => setComparePasswordInput(e.target.value)}></input>
                    <input className='input' required placeholder='Электронная почта' type="email" onChange={(e) => setEmailInput(e.target.value)}></input>
                    <p className='error-message'>{errorMessage}</p>
                    <button type='submit' className='login-button'>Регестрация</button>
                    
                </form>
                <Link to='/auth/login'><div className='reg-button'>Уже есть аккаунт</div></Link>
            </div>
        </div>
    )

}

export default observer(RegisterForm)