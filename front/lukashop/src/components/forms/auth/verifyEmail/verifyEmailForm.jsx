import './style.css'
import { useState } from 'react'
import { useStore } from '../../../../stores/clientStore'

function VerifyEmailForm({verifyCode}) {
    const [inputVeriftyCode, setInputVerifyCode] = useState("")
    const [errorMessage, setErrorMessage] = useState("")
    const clientStore = useStore().ClientStore 
    const checkVerifyCode = (e) => {
        e.preventDefault()
        if (inputVeriftyCode == clientStore.emailVerifyCode) {
            clientStore.sendNofication("Ваш Email успешно подтвержден!")
            clientStore.setVerifyEmail()
        } else {
            setErrorMessage("Неверный код")
        }
    }
    return (<div>
        <form onSubmit={(e) => checkVerifyCode(e)}>
            <div className='info'>Код подтверждения был отправлен на почту {clientStore.email}</div><br />
            <input type='text' placeholder='Проверочный код' className='verify-field' onChange={(e) => setInputVerifyCode(e.target.value)}></input><br />
            {errorMessage}
        </form>
    </div>)
}


export default VerifyEmailForm