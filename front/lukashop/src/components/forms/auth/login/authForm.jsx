import { observer } from 'mobx-react'
import './style.css'
import { useStore } from '../../../../stores/clientStore'
import { useState } from 'react'
import { Link } from 'react-router-dom'

function AuthForm() {
    const clientStore = useStore().ClientStore 
    const [loginInput, setLoginInput] = useState("")
    const [passwordInput, setPasswordInput] = useState("")
    const [errorMessage, setErrorMessage] = useState("")
    return (
        <div className='authForm-wrapper'>
            <div className='authForm'>
                <h1 className='title'>Вход</h1>
                <form onSubmit={(e) => clientStore.login(e, loginInput, passwordInput, setErrorMessage)} className='input-wrapper'>
                    <input className='input' required placeholder='Логин' onChange={(e) => setLoginInput(e.target.value)}></input>
                    <input className='input' required placeholder='Пароль' type="password" onChange={(e) => setPasswordInput(e.target.value)}></input>
                    <p className='error-message'>{errorMessage}</p>
                    <button type='submit' className='login-button'>Войти</button>
                    
                </form>
                <Link to='/auth/register'><div className='reg-button'>Регестрация</div></Link>
            </div>
        </div>
    )

}

export default observer(AuthForm)