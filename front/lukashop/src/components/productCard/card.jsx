import './style.css'
import { Link } from 'react-router-dom'
import { mediaIp } from '../../requestCofing'

function ProductCard({product}) {
    return (
        <div className='card-wrapper'>
            <div className='card-info'>
                <img className='image' src={`${mediaIp}/${product.image}`}></img>
                <div className='card-text-info'>
                    <div className='name'><h6>{product.name}</h6></div>
                    <div className='cost'><h6>Цена: {product.cost}р</h6></div>
                </div>
            </div>
            <Link to={`/shops/${product.idShop}/products/${product.id}`}><div className='buy-button'><h4>Купить</h4></div></Link>
        </div>
    )

}


export default ProductCard