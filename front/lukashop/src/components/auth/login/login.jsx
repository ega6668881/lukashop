import { observer } from "mobx-react";
import AuthForm from "../../forms/auth/login/authForm";
import { useStore } from "../../../stores/clientStore";
import { Navigate } from "react-router-dom";


function Login(props) {
    const clientStore = useStore().ClientStore 
    if (clientStore.isLogin) {
        return <Navigate to={`/profile/${clientStore.idUser}`} />
    }
    return (<div className="login"><AuthForm /></div>)

}

export default observer(Login)