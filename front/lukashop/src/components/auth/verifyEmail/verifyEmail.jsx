import { observer } from 'mobx-react'
import './style.css'
import { useState, useEffect } from 'react'
import { useStore } from '../../../stores/clientStore'
import { Navigate } from 'react-router-dom'
import VerifyEmailForm from '../../forms/auth/verifyEmail/verifyEmailForm'
import loading from '../../media/loading.gif'

function VerifyEmail(props) {
    const clientStore = useStore().ClientStore 
    const [emailSend, setEmailSend] = useState(false)
    const [verifyCode, setVerifyCode] = useState(null)

    if (clientStore.emailVerif) {
        return <Navigate to={`/profile/${clientStore.idUser}`}/>
    } else {
        clientStore.getVerifyEmail()
        console.log(clientStore.emailVerifyCode)
        return (<div className='verify-wrapper'>
            <div className='verify'>
                {clientStore.emailVerifyCode ? (<VerifyEmailForm verifyCode={verifyCode}/>) : (
                    <div className='send-mail-wrapper'>
                        <div className='info'>Для подтверждения Email вам придет уведомление на электронную почту с кодом подтверждения</div>
                        <div className='send-mail' onClick={(e) => clientStore.sendEmailCode(setEmailSend)}>Отправить код {emailSend && (<img src={loading} width='70px' height='20px'></img>)}</div>
                    </div>
                )}
            </div>
        </div>)
    }
    
    
}


export default observer(VerifyEmail)