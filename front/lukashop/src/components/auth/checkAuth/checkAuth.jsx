import { observer } from 'mobx-react'
import './style.css'
import { Navigate } from 'react-router-dom'
import { useStore } from '../../../stores/clientStore'

function CheckAuth({Component}) {
    const clientStore = useStore().ClientStore 
    clientStore.checkToken()
    if (clientStore.isLogin) {
        return Component
    } else {
        return (<Navigate to={'/auth/login'} />)
    }
}

export default observer(CheckAuth)