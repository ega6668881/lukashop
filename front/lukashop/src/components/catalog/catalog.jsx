import { observer } from "mobx-react";
import './style.css'
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useCatalogStore } from "../../stores/catalogStore";

function Catalog() {
    const {category} = useParams()
    const catalogStore = useCatalogStore().CatalogStore
    if (catalogStore.category != category) {
        catalogStore.category = category
        catalogStore.getProducts()
    }
    
    if (catalogStore.products) {
        return (<div className="catalog-wrapper">{catalogStore.products}</div>)
    } else if (!catalogStore.products) {
        catalogStore.getProducts()
        return (<div>Загрузка</div>)
    } else if (catalogStore.products.length == 0) {
        return (<div>Товары не найдены!</div>)
    }
}

export default observer(Catalog)