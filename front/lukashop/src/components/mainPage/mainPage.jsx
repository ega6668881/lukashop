
import './style.css'
import ProductCard from '../productCard/card'
import { useMainPageStore } from '../../stores/mainPageStore'
import { useState } from 'react'
import { observer } from 'mobx-react'

function MainPage() {
    const mainPageStore = useMainPageStore().MainPageStore
    let marketingProducts = []
    mainPageStore.marketingProducts.forEach(product => {
        marketingProducts.push(<ProductCard product={product} />)
    })
    return (
        <div className="main-page-wrapper">
            <div className='rec-wrapper'>
                <h1 className='rec-title'>LUKA SHOP РЕКОМЕНДУЕТ</h1>
                <div className='rec-card-wrapper'>
                    {marketingProducts}
                </div>
            </div>
        </div>
    )
}

export default observer(MainPage)