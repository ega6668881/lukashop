import { observer } from 'mobx-react'
import './style.css'
import { Link, useParams } from 'react-router-dom'
import { useStore } from '../../stores/clientStore'
import { useEffect, useState } from 'react'
import { ip, mediaIp } from '../../requestCofing'

function Profile(props) {
    const {id} = useParams()
    const clientStore = useStore().ClientStore
    const [isEdit, setEdit] = useState(false)
    const [user, setUser] = useState(null)
    useEffect(() => {
        fetch(`${ip}/users/get-user`, {
            method: "GET",
            headers: {'Content-Type': 'application/json', 'iduser': id},
        }).then(response => {
            console.log(response)
            if (response.status === 200) {
                response.json().then(result => {
                    result.user.createdAt = new Date(result.user.createdAt).toLocaleDateString(undefined, {year: 'numeric', month: 'long', day: 'numeric'})
                    setUser(result.user)
                    if (clientStore.idUser === result.user.id) setEdit(true)
                })
            }
        })
         
    }, [])

    return (
        <div className='profile-wrapper'>
            <div className='profile'>
                {user ? (<img className='avatar' src={`${mediaIp}/${user.avatar}`}></img>) : (<div className='avatar'></div>)}
                {isEdit && (<form><input type='file' className='avatar-edit' onChange={(e) => clientStore.avatarUpload(e, setUser, e.target.files[0])}></input></form>)}
                <div className='user-info-wrapper'>
                    {user ? (<>
                        <h1 className='user-info'>{user.login}</h1>
                        {isEdit && (<h2 className='user-info'>Баланс: {user.balance}р.</h2>)}
                        {isEdit && (<Link to='/donate'><h2 className='donate-button'>Пополнить баланс</h2></Link>)}
                        <h2 className='user-info'>На lukashop с: {user.createdAt}</h2>
                        {isEdit && <h2 className='user-info'>Email: {clientStore.email}</h2>}</>) : (
                        <div>Загрузка</div>
                    )}
                    {isEdit ? (<>{clientStore.emailVerif ? (<div>Email подтвержден</div>) : (<Link to="/auth/verify-email"><div>Подтвердить email</div></Link>)}</>) : (<></>)}
                </div>
                {isEdit && (
                    <div className='buttons-wrapper'>
                        <Link to='/history-paynament'><button className='button'>История пополнений</button></Link>
                        <Link to='/buy-products'><button className='button'>Мои покупки</button></Link>
                    </div>
                )}
            </div>
        </div>
    )
}

export default observer(Profile)