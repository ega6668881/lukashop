import './style.css'


function Paynament({paynament}) {
    return (
        <div className='paynament'>
            <div className='amount field'>Сумма: {paynament.amount}р</div>
            <div className='date field'>Дата: {new Date(paynament.createdAt).toLocaleDateString(undefined, {year: 'numeric', month: 'long', day: 'numeric', hour: '2-digit', minute: '2-digit'})}</div>
            
        </div>
    )
}

export default Paynament