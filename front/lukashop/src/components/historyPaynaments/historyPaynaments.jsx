import { observer } from 'mobx-react'
import './style.css'
import { useStore } from '../../stores/clientStore'
import { usePaynamentHistoryStore } from '../../stores/paynamentHistoryStore'
import Paynament from './paynament/paynament'

function HistoryPaynaments() {
    const clientStore = useStore().ClientStore
    const paynamentHistoryStore = usePaynamentHistoryStore().PaynamentHistory

    if (paynamentHistoryStore.paynaments) {
        let paynaments = []
        paynamentHistoryStore.paynaments.forEach(paynament => {
            paynaments.push(<Paynament paynament={paynament}/>) 
        })

        return (<div className='paynaments-wrapper'>
            <div className='paynaments'>{paynaments}</div>
        </div>)

    } else {
        paynamentHistoryStore.getPaynaments(clientStore.idUser)
        return (<div>Загрузка</div>)
    }
}

export default observer(HistoryPaynaments)