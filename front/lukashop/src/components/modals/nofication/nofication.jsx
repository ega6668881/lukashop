import './style.css'


function Nofication({content}) {

    return (<div className='nofication-wrapper'>
        <div className='nofication'>
            <div className='content'>{content}</div>
        </div>
    </div>)

}

export default Nofication