import { observer } from 'mobx-react'
import './style.css'
import { Link, useParams } from 'react-router-dom'
import { ip, mediaIp } from '../../requestCofing'
import sellsImage from '../media/cart.png'
import bubbleImage from '../media/bubble.png'
import { useProductAboutStore } from '../../stores/productAboutStore'
import { useStore } from '../../stores/clientStore'

function AboutProduct(props) {
    const {id} = useParams()
    const productAboutStore = useProductAboutStore().productAboutStore 
    const clientStore = useStore().ClientStore
    if (productAboutStore.product && productAboutStore.idProduct == id) {
        return (
            <div className='about-product-wrapper'>
                <div className='about-product'>
                    <div className='title'>{`Купить ${productAboutStore.product.name}`}</div>
                    <div className='header-product-wrapper'>
                        <img className='image' src={`${mediaIp}/${productAboutStore.product.image}`} alt='image'></img>
                        <div className='buy-wrapper'>
                            <div className='cost'>{productAboutStore.product.cost}р.</div>
                            <div className='buy-button' onClick={(e) => productAboutStore.buyProduct(clientStore)}>Купить</div>
                            <div className='report-button'>Пожаловаться</div>
                        </div>
                    </div>
                    <div className='info-buttons-wrapper'>
                        <Link to={`/products/feedbacks/${productAboutStore.product.id}`}><div className='info-button'><img src={bubbleImage} alt='feedbacks'></img>{productAboutStore.feedbacks.length} отзывов</div></Link>
                        <div className='info-button'><img src={sellsImage} alt='sells'></img>{productAboutStore.product.sellCount} продаж</div>
                        {/* <div className='info-button'>Оставить отзыв</div> */}
                        <Link to={`/shops/${productAboutStore.shop.id}`}><div className='shop-button'>Магазин {productAboutStore.shop.name}</div></Link>
                    </div>
                    <div className='description-wrapper'>
                        <div className='title'>Описание товара</div>
                        <div className='description'>{productAboutStore.product.description}</div>
                    </div>
                </div>
            </div>
        )
    }
    else {
        productAboutStore.getProduct(id)
        return (<div>Загрузка</div>)
    }
    
}


export default observer(AboutProduct)