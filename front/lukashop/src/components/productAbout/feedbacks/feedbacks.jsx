import { observer } from "mobx-react";
import { useProductAboutStore } from "../../../stores/productAboutStore";
import { Link, useParams } from "react-router-dom";
import FeedBack from "./feedback/feedback";
import './style.css'

function Feedbacks({feedbacks}) {
    
    const productAboutStore = useProductAboutStore().productAboutStore
    const {idProduct} = useParams()
    if (productAboutStore.notFound) {
        return (<div>Продукт не найден</div>)
    }

    if (productAboutStore.product) {
        let feedbacks = []
        productAboutStore.feedbacks.forEach(feedback => {
            feedbacks.push(<Link to={`/profile/${feedback.idUser}`}><FeedBack feedback={feedback}/></Link>)
        })
        return (
            <div className="feedbacks-wrapper">
                <div className="feedbacks">
                    <h1 className="title">Отзывы о товаре {productAboutStore.product.name}</h1>
                    <div className="productAbout-wrapper">
                        <div className="product-about">

                        </div>
                    </div>
                    {feedbacks}
                </div>
            </div>
        )
    } else {
        productAboutStore.getProduct(idProduct)
        return (<div>Загрузка</div>)
    }
    
}

export default observer(Feedbacks)