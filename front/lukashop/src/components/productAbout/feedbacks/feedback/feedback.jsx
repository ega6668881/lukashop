import { observer } from 'mobx-react'
import './style.css'
import bubbleImage from '../../../media/bubble.png'
import redScore from '../../../media/red-score.png'
import greenScore from '../../../media/green-score.png'

function FeedBack({feedback}) {
    return (
        <div className='feedback'>
            <div className='feedback-score'>
                <img src={feedback.score ? (greenScore) : (redScore)} className='feedback-score' alt='score-image'></img>
            </div>
            <div className='feedback-message'>{feedback.message}</div>
            <div className='feedback-user-avatar'></div>
        </div>
    )
}

export default FeedBack