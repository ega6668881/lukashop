import { observer } from 'mobx-react'
import './style.css'
import { useSellerProfileStore } from '../../stores/sellerProfileStore'
import { useStore } from '../../stores/clientStore'
import SellerProducts from './products/products'
import SellerMessenger from './messenger/messenger'
import AddProduct from './products/addProduct/addProduct'

function SellerProfile(props) {
    const sellerProfileStore = useSellerProfileStore().sellerProfileStore
    const clientStore = useStore().ClientStore
    const visibleComponent = {
        'products': <SellerProducts />,
        'chat': <SellerMessenger />,
        'add-product': <AddProduct />
    }
    if (sellerProfileStore.shop) {
        return (
            <div>
                <div className='seller-profile-wrapper'>
                    <div className='seller-profile'>
                        <div className='nav-wrapper'>
                            <div className='nav'>
                                <div className={sellerProfileStore.page == 'products' ? 'item active start': 'item start'} onClick={(e) => sellerProfileStore.page = 'products'}>Товары</div>
                                {/* <div className={sellerProfileStore.page == 'analitika' ? 'item active': 'item'} onClick={(e) => sellerProfileStore.page = 'analitika'}>Аналитика</div> */}
                                <div className={sellerProfileStore.page == 'chat' ? 'item active': 'item'} onClick={(e) => sellerProfileStore.page = 'chat'}>Чат</div>
                                {/* <div className={sellerProfileStore.page == 'support' ? 'item active': 'item'} onClick={(e) => sellerProfileStore.page = 'analitika'}>Поддержка</div>
                                <div className={sellerProfileStore.page == 'finance' ? 'item active': 'item'} onClick={(e) => sellerProfileStore.page = 'analitika'}>Финансы</div>
                                <div className={sellerProfileStore.page == 'office' ? 'item active end': 'item end'} onClick={(e) => sellerProfileStore.page = 'analitika'}>Кабинет</div> */}
                            </div>
                        </div>
                        <div>{sellerProfileStore.page && (<div>{visibleComponent[sellerProfileStore.page]}</div>)}</div>
                    </div>
                </div>
           
            </div>
        )
    } else {
        sellerProfileStore.getShop(clientStore.idUser)
        return(<div>Загрузка</div>)
    }
    
}

export default observer(SellerProfile)