import { observer } from 'mobx-react'
import './style.css'
import { useSellerProfileStore } from '../../../../stores/sellerProfileStore'

function SellerMessage({message}) {
    const sellerProfileStore = useSellerProfileStore().sellerProfileStore
    return (<div className={message.idUser === sellerProfileStore.activeChat.idSeller ? ('message-wrapper shop') : ('message-wrapper user')}>
        <div className='message'>
            {message.message}
        </div>
    </div>)
}

export default observer(SellerMessage)