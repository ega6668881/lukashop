import './style.css'
import { useSellerProfileStore } from '../../../stores/sellerProfileStore'
import { observer } from 'mobx-react'
import { mediaIp } from '../../../requestCofing'
import SellerMessage from './message/message'
import { useRef, useState } from 'react'

function SellerMessenger(props) {
    
    const sellerProfileStore = useSellerProfileStore().sellerProfileStore
    const messagesEndRef = useRef(null)
    const [messageInput, setMessageInput] = useState("")
    const scrollToBottom = () => {
        messagesEndRef.current?.scrollIntoView({ behavior: "smooth" })
    }
    
    if (sellerProfileStore.chatsList) {    
        let messagesList = []
        sellerProfileStore.chatsList.forEach(chat => {
            messagesList.push(
                <div className='message-in-list' onClick={() => sellerProfileStore.getMessages(chat)}>
                    <div className='avatar-wrapper'><img className='avatar' src={`${mediaIp}/${chat.user.avatar}`} alt='avatar'></img></div>
                    <div className='message-info'>
                        <div className='username'>{chat.user.login}</div>
                        <div className='last-message'>{chat.lastMessage.message.length > 5 ? (<>{chat.lastMessage.message.substring(0, 7) + '...'}</>) : (<>{chat.lastMessage.message}</>)}</div>
                    </div>
                </div>
            )
        })
        let chatMessages = []
        if (sellerProfileStore.activeChat && sellerProfileStore.messages) {
            sellerProfileStore.messages.forEach(message => {
                chatMessages.push(<SellerMessage message={message}/>)
            })
            scrollToBottom()
        }
        return (
            <div className='seller-messenger-wrapper'>
                <div className='seller-messenger'>
                    <div className='messages-list-wrapper'>
                        <div className='messages-list'>{messagesList}</div>
                    </div>
                    <div className='chat-wrapper'>
                        {sellerProfileStore.activeChat && (
                            <div className='chat'>
                                <div className='product-info-wrapper'>
                                    <div className='product-info'>
                                        <div className='image-wrapper'><img className='image' src={`${mediaIp}/${sellerProfileStore.activeChat.product.image}`} alt='product-image'></img></div>
                                        <div className='name'>{sellerProfileStore.activeChat.product.name}</div>
                                    </div>
                                </div>
                                <div className='messages'>
                                    {chatMessages}
                                    <div ref={messagesEndRef}></div>
                                </div>
                                <div className='send-message-wrapper'>
                                    <form onSubmit={(e) => sellerProfileStore.sendMessage(e, messageInput, scrollToBottom)}><input placeholder='Введите текст сообщения' className='send-message' onChange={(e) => setMessageInput(e.target.value)}></input></form>
                                    
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        )
    } else {
        sellerProfileStore.getChats()
        return (<div>Загрузка</div>)
    }
}

export default observer(SellerMessenger)