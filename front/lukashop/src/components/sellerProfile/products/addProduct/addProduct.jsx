import './style.css'
import { useStore } from '../../../../stores/clientStore'
import { useSellerProfileStore } from '../../../../stores/sellerProfileStore'
import { observer } from 'mobx-react'
import { useState } from 'react'

function AddProduct() {
    const clientStore = useStore().ClientStore
    const sellerProfileStore = useSellerProfileStore().sellerProfileStore
    const [productName, setProductName] = useState('')
    const [productDescription, setProductDescription] = useState('')
    const [productCost, setProductCost] = useState('')
    const [product, setProduct] = useState('')
    const [productImage, setProductImage] = useState()
    const [productCategory, setProductCategory] = useState('')
    return (
        <div className='add-product-wrapper'>   
            <form className='add-product-form' onSubmit={(e) => sellerProfileStore.createProduct(e, productName, product, productDescription, productImage, productCost, productCategory)}>
                <input placeholder='Введите название продукта' required className='field' onChange={(e) => setProductName(e.target.value)}></input>
                <textarea placeholder='Введите описание продукта' required className='field' onChange={(e) => setProductDescription(e.target.value)}></textarea>
                <textarea placeholder='Укажите выдаваемый продукт в виде строки, продукты разделяйте символом "~". Пример: Продукт1~Продукт2' required className='field' onChange={(e) => setProduct(e.target.value)}></textarea>
                <input placeholder='Введите цену продукта' required className='field' onChange={(e) => setProductCost(e.target.value)}></input>
                Выберите категорию продукта
                <select name='category' className='field category-select' size="7" multiple required onChange={(e) => setProductCategory(e.target.value)}>
                    <option value='discord-nitro' className='select-option'>Discord Nitro</option>
                    <option value='keys' className='select-option'>Ключи</option>
                    <option value='accounts' className='select-option'>Аккаунты</option>
                    <option value='items' className='select-option'>Предметы</option>
                    <option value='other' className='select-option'>Другое</option>
                </select>
                Укажите изображение для продукта
                <input type='file' className='field' required onChange={(e) => setProductImage(e.target.files[0])}></input>
                <button type='submit' className='button'>Добавить товар</button>
            </form>
        </div>
    )

}

export default observer(AddProduct)