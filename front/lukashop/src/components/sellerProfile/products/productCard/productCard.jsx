import './style.css'
import { mediaIp } from '../../../../requestCofing'
import redScore from '../../../media/red-score.png'
import greenScore from '../../../media/green-score.png'

function SellerProductCard({product}) {
    let badFeedBacks = 0
    let goodFeedBacks = 0
    let visible = null
    switch (product.visible) {
        case true: visible = "Активен"; break
        case null: visible = "На проверке"; break
        case false: visible = "Не активен"; break
    }
    if (product.feedBacks) {
        product.feedBacks.forEach(feedBack => {
            if (feedBack.score) {
                goodFeedBacks += 1
            } else {
                badFeedBacks += 1
            }
        })
    }
    
    return (
        <table className='seller-product-card'>
            <tr className='table-row'>
                <td className='image-wrapper start'><img src={`${mediaIp}/${product.image}`} className='image' width="198px" height="120px"></img></td>
                <td className='name'>{product.name}</td>
                <td className='feedbacks'><img src={redScore}></img>{goodFeedBacks}</td>
                <td className='feedbacks'><img src={greenScore}></img>{badFeedBacks}</td>
                <td className='status'>{visible}</td>
                <td className='products-count end'>{product.productCount}шт</td>
            </tr>
        </table>
    )
}


export default SellerProductCard