import { observer } from 'mobx-react'
import './style.css'
import { useSellerProfileStore } from '../../../stores/sellerProfileStore'
import SellerProductCard from './productCard/productCard'

function SellerProducts(props) {
    const sellerProfileStore = useSellerProfileStore().sellerProfileStore
    if (sellerProfileStore.products) {
        let products = []
        sellerProfileStore.products.forEach(product => {
            products.push(<SellerProductCard product={product}/>)
        })
        return (
            <div>
                <div className='seller-product-card-wrapper'>
                    {products}
                    
                </div>
                <div className='add-product-button-wrapper'>
                    <div className='add-product-button' onClick={(e) => sellerProfileStore.page = 'add-product'}>Добавить продукт</div>
                </div>
            </div>
        )
    } else {
        sellerProfileStore.getProducts()
        return (<div>Загрузка</div>)
    }
}


export default observer(SellerProducts)