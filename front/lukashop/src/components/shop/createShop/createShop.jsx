import './style.css'
import { useStore } from '../../../stores/clientStore'
import { Navigate } from 'react-router-dom'
import { observer } from 'mobx-react'
import { useState } from 'react'

function CreateShop(props) {
    const clientStore = useStore().ClientStore
    const [name, setName] = useState()
    const [description, setDescription] = useState()
    const [file, setFile] = useState()
    if (!clientStore.emailVerif) {
        return (<Navigate to='/auth/verify-email'/>)
    } else {
        if (clientStore.shop) {
            return <Navigate to="/seller-profile"/>
        }
        return (<div className='create-shop-wrapper'>
                <form className='create-shop-form' onSubmit={(e) => clientStore.createShop(e, name, description, file)}>
                    Создание магазина
                    <input className='name field' onChange={(e) => setName(e.target.value)} placeholder='Введите название магазина' required></input>
                    <textarea className='description field' type='text-area' onChange={(e) => setDescription(e.target.value)} required placeholder='Введите описание магазина'></textarea>
                    <h6 className='banner-title'>Баннер магазина{file && (<img src={file.path}></img>)}</h6>
                    <input type='file' className='banner field' name='banner' onChange={(e) => setFile(e.target.value)} required></input>
                    <button type='submit' className='button field'>Создать магазин</button>
                </form>
        </div>)
    }
}

export default observer(CreateShop)