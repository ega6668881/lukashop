import { observer } from 'mobx-react'
import './style.css'
import { useShopStore } from '../../../stores/shopStore'
import { useParams } from 'react-router-dom'
import { mediaIp } from '../../../requestCofing'
import redScore from '../../media/red-score.png'
import greenScore from '../../media/green-score.png'
import sellsImage from '../../media/cart.png'
import ProductCard from '../../productCard/card'

function ShopAbout(props) {
    const shopStore = useShopStore().ShopStore
    const {idShop} = useParams()
    if (shopStore.shop) {
        let popularProducts = []
        shopStore.popularProducts.forEach(product => {
            popularProducts.push(<ProductCard product={product}></ProductCard>)
        })
        return (
            <div className='shop-about-wrapper'>
                <div className='shop-about'>
                    <div className='banner-wrapper'><img className='banner' alt='banner' src={`${mediaIp}/${shopStore.shop.banner}`}></img></div>
                    <div className='info-buttons-wrapper'>
                        <div className='info-button'><img src={greenScore} width='40px' height='40px'></img>{shopStore.goodFeedBacks.length} Отзывов</div>
                        <div className='info-button'><img src={redScore} width='40px' height='40px'></img>{shopStore.badFeedBacks.length} Отзывов</div>
                        <div className='name'>{shopStore.shop.name}</div>
                    </div>
                    <div className='popular-products-wrapper'>
                        <div className='title'>Популярные товары</div>
                        <div className='popular-products'>
                            {popularProducts}
                        </div>
                    </div>
                </div>
            </div>
        )
    } else {
        shopStore.getShop(idShop)
        return (<div>Загрузка</div>)
    }
}

export default observer(ShopAbout)