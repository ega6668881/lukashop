import { observer } from 'mobx-react'
import './style.css'
import { useStore } from '../../stores/clientStore'


function Donate(props) {
    const clientStore = useStore().ClientStore
    const onSubmit = (e) => {
        fetch(`http://89.23.100.86:3000/api/v1/paynaments/create-paynament`, {
            method: "POST",
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                idUser: clientStore.idUser,
                label: e.target.label.value,
                amount: e.target.sum.value,
            })})
    }
    return (
        <div className='paynament-wrapper'>
            <form method='POST' onSubmit={(e) => onSubmit(e)} action="https://yoomoney.ru/quickpay/confirm.xml" className='paynament-form'>
                <input type="hidden" name="receiver" value="410017459068001"/>
                <input type="hidden" name="label" value={`${clientStore.idUser}`}/>
                <input type="hidden" name="quickpay-form" value="button"/>
                <input name="sum" data-type="number" placeholder='Сумма пополнения' className='amount field'/>
                <label><input type="radio" name="paymentType" value="PC" className='method-select field'/>ЮMoney</label>
                <label><input type="radio" name="paymentType" value="AC" className='method-select'/>Банковской картой</label>
                <input type="submit" value="Пополнить" className='submit-button field' />
            </form>
        </div>
    )

}

export default observer(Donate)