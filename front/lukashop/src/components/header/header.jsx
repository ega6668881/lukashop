import {Link} from 'react-router-dom'
import './header.css'
import searchButton from '../media/search-button.svg'
import user from '../media/User.svg'
import SingIn from '../media/SingIn.svg'
import { observer } from 'mobx-react'
import { useStore } from '../../stores/clientStore';
import { useMainPageStore } from '../../stores/mainPageStore'
import { useState } from 'react'
import Nofication from '../modals/nofication/nofication'

function Header(props) {
    const clientStore = useStore().ClientStore 
    const [searchInput, setSearchInput] = useState()
    return (<div className='header-wrapper'>
        {clientStore.nofication && (<Nofication content={clientStore.nofication}></Nofication>)}
        <div><Link to="/" className='logo'>LUKA SHOP</Link></div>
        <div className="head">
            <div className='search-wrapper'>
                <form action={`/search/${searchInput}`}>
                    <input className="search" placeholder='Поиск среди тысячи товаров' onChange={(e) => setSearchInput(e.target.value)}></input>
                </form>
                <div className="search-button"><img src={searchButton}></img></div>
            </div>
            {clientStore.isLogin ? (<div className='login'><Link to={`profile/${clientStore.idUser}`}><img src={user} width="59px" height = "59px"></img></Link></div>) : (<div className='login'><Link to="auth/login"><img src={SingIn} width="59px" height = "59px"></img></Link></div>)}
            {clientStore.shop ? (<div className='makeSeller'><Link to='/seller-profile'>Личный кабинет</Link></div>) : (<div className='makeSeller'><Link to='/shops/create-shop'>Стать продавцом</Link></div>)}
            
            
        </div>
        <div className='nav-wrapper'>
            <div className = "nav-bar">
                <Link to="/catalog/discord-nitro"><div className={clientStore.page == 'nitro' ? 'active': 'nav'}>Discord Nitro</div></Link>
                <Link to='/catalog/accounts'><div className={clientStore.page == 'accounts' ? 'active': 'nav'}>Аккаунты</div></Link>
                <Link to="/catalog/keys"><div className={clientStore.page == 'keys' ? 'active': 'nav'}>Ключи</div></Link>
                <Link to="/catalog/items"><div className={clientStore.page == 'items' ? 'active': 'nav'}>Предметы</div></Link>
                <Link to="/catalog/other"><div className={clientStore.page == 'other' ? 'active': 'nav'}>Другое</div></Link>
            </div>
        </div>
    </div>)
}

export default observer(Header)