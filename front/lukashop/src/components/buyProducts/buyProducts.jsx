import { observer } from 'mobx-react'
import './style.css'
import { useStore } from '../../stores/clientStore'
import BuyProductCard from './buyProductCard/card'
import BuyProductAbout from './buyProductAbout/buyProductAbout'
import { useState } from 'react'

function BuyProducts(props) {
    const clientStore = useStore().ClientStore
    const [productAbout, setProductAbout] = useState(null)
    if (clientStore.buyProducts) {
        let buyProducts = []
        console.log(productAbout)
        if (productAbout) {
            return (<BuyProductAbout product={productAbout}/>)

        } else {
            clientStore.buyProducts.forEach(buyProduct => {
                buyProducts.push(<div onClick={((e) => {setProductAbout(buyProduct)})}><BuyProductCard product={buyProduct}/></div>)
            })
            return (
                <div className='buy-products-wrapper'>
                    <div className='buy-products'>
                        {buyProducts}
                    </div>
                </div>
            )
        }
        

    } else {
        clientStore.getBuyProducts()
        return (<div>Загрузка</div>)
    }
}


export default observer(BuyProducts)