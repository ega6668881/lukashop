import { observer } from 'mobx-react'
import './style.css'
import { mediaIp } from '../../../requestCofing'
import { useBuyProductStore } from '../../../stores/buyProductAboutStore'
import Message from './message/message'
import { useState, useRef } from 'react'
import { useStore } from '../../../stores/clientStore'

function BuyProductAbout({product}) {
    const buyProductsStore = useBuyProductStore().BuyProductStore
    const clientStore = useStore().ClientStore
    const [messageInput, setMessageInput] = useState('')
    const messagesEndRef = useRef(null)
    const scrollToBottom = () => {
        messagesEndRef.current?.scrollIntoView({ behavior: "smooth" })
    }
    if (buyProductsStore.product && buyProductsStore.product.id == product.id) {
        let messages = []
        if (buyProductsStore.messages.length > 0) {
            buyProductsStore.messages.forEach(message => {
                messages.push(<Message message={message}/>)
                
            })
            scrollToBottom()
            
        } else {
            if (buyProductsStore.idChat) {
                buyProductsStore.getMessages()
            }
            
        }
        return (
            <div className='buy-product-about-wrapper'>
                <div className='buy-product-about'>
                    <div className='image-wrapper'><img className='image' src={`${mediaIp}/${product.productInfo.image}`} alt='product-image'></img></div>
                    <div className='product-name'>{buyProductsStore.product.productInfo.name}</div>
                    <div className='product'>Никому не передавайте купленый ваш товар!<p className='product-text'>{product.product}</p>
                        <div className='cost'>Вы купили товар за: {product.cost}р</div>
                        <div className='description'>{buyProductsStore.product.productInfo.description}</div>
                        <div className='messanger-wrapper'>
                            Чат с продавцом
                        <div className='messanger'>
                            <div className='chat'>
                                {messages}
                                <div ref={messagesEndRef}></div>
                            </div>
                            <form className='send-message-wrapper' onSubmit={(e) => {buyProductsStore.sendMessage(e, messageInput, clientStore.idUser, scrollToBottom)}}>
                                <input className='send-message-input' placeholder='Введите текст сообщения' onChange={(e) => {setMessageInput(e.target.value)}}></input>
                            </form>
                        </div>
                    </div>
                    </div>
                    
                </div>
            </div>
        )
    } else {
        buyProductsStore.init(product)
        return (<div>Загрузка</div>)
    }
}

export default observer(BuyProductAbout)