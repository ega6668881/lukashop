import { observer } from 'mobx-react'
import './style.css'
import { useStore } from '../../../../stores/clientStore'
import { useBuyProductStore } from '../../../../stores/buyProductAboutStore'

function Message({message}) {
    const clientStore = useStore().ClientStore
    const buyProductStore = useBuyProductStore().BuyProductStore
    return (<div className={message.idUser === buyProductStore.product.productInfo.idUser ? ('message-wrapper shop') : ('message-wrapper user')}>
        <div className='message'>
            {message.message}
        </div>
    </div>)
}

export default observer(Message)