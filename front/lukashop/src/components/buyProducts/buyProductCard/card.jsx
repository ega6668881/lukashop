import './style.css'
import { mediaIp } from '../../../requestCofing'



function BuyProductCard({product}) {

    return (<div className='buy-product-card'>
        <div className='image-wrapper'><img className='image' src={`${mediaIp}/${product.productInfo.image}`} alt='product-image'></img></div>
        <div className='name'>{product.productInfo.name}</div>
    </div>)

}

export default BuyProductCard