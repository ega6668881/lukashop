import './style.css'
import { useParams } from 'react-router-dom'
import ProductCard from '../productCard/card'
import { useEffect, useState } from 'react'
import { ip } from '../../requestCofing'

function Search() {
    const {searchItem} = useParams()
    const [products, setProducts] = useState(null)
    useEffect(() => {
        const headers = new Headers()
        headers.append('Content-Type', 'application/json')
        headers.append('search', encodeURIComponent(searchItem.toString()))
        fetch(`${ip}/products/get-product`, {
            method: "GET",
            headers: headers
        }).then(response => {
            if (response.status == 200) {
                response.json().then(result => {
                    console.log(result)
                    if (result.result.length == 0) {
                        setProducts([])
                    } else {
                        let tempProducts = []
                        result.result.forEach(product => {
                            tempProducts.push(<ProductCard product={product}/>)
                        })
                        setProducts(tempProducts)
                    }
                })
            }
        })
    }, [])

    if (products) {
        return (<div className="catalog-wrapper">{products}</div>)
    } else if (!products) {
        return (<div>Загрузка</div>)
    } else if (products.length === 0) {
        return (<>Товары не найдены</>)
    }
    

}

export default Search