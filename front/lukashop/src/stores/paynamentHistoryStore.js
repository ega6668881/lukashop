import { makeAutoObservable, observable, action, autorun } from "mobx";
import { createContext, useContext } from "react";
import {ip} from '../requestCofing.js'
import ProductCard from "../components/productCard/card.jsx";


class PaynamentHistory {
    paynaments = null
    constructor () {
        makeAutoObservable(this)
    }
    getPaynaments = action((idUser) => {
        fetch(`${ip}/paynaments/get-paynaments`, {
            method: "GET",
            headers: {'Content-Type': 'application/json', 'iduser': idUser}
        }).then(response => {
            if (response.status == 200) {
                response.json().then(result => {
                    this.paynaments = result.paynaments
                })
                
            }
        })
    })
}


const paynamentHistory =  {
    PaynamentHistory: new PaynamentHistory(),
}

export const StoreContext = createContext(paynamentHistory);

export const usePaynamentHistoryStore = () => {
  return useContext(StoreContext);
};

export default PaynamentHistory;