import { makeAutoObservable, observable, action } from "mobx";
import { createContext, useContext } from "react";
import {ip} from '../requestCofing.js'

class ProductAboutStore {
    product = null
    feedbacks = null
    idProduct = null
    shop = null
    notFound = false
    constructor () {
        makeAutoObservable(this)
    }

    buyProduct = action((clientStore) => {
        if (!clientStore.isLogin) {
            clientStore.sendNofication("Вы должны авторизоваться для покупки товара.")
        } else {
            if (clientStore.balance < this.product.cost) {
                return clientStore.sendNofication("У вас недостаточно денег на балансе!")
            }
            fetch(`${ip}/shops/buy-product`, {
                method: "POST",
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify({
                    idUser: clientStore.idUser,
                    idProduct: this.idProduct,
                })
            }).then(response => {
                if (response.status === 200) {
                    response.json().then(result => {
                        clientStore.getBuyProducts()
                        clientStore.sendNofication("Купленный товар отобразился у вас в истории покупок")
                    })

                } else {
                    return clientStore.sendNofication("У вас недостаточно денег на балансе!")
                }
            })
        }
    })

    getProduct = action((idProduct) => {
        fetch(`${ip}/products/get-product`, {
            method: "GET",
            headers: {'Content-Type': 'application/json', 'idproduct': idProduct}
        }).then(response => {
            if (response.status == 200) {
                response.json().then(result => {
                    this.product = result.product
                    if (!this.product) {
                        this.notFound = true
                    } else {
                        this.idProduct = result.product.id
                        this.feedbacks = result.feedbacks
                        this.shop = result.shop
                    }
                })
            }
        })
    })
}


const productAboutStore = {
    productAboutStore: new ProductAboutStore(),
}

export const StoreContext = createContext(productAboutStore);

export const useProductAboutStore = () => {
  return useContext(StoreContext);
};

export default ProductAboutStore;