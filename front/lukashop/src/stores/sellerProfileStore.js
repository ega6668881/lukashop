import { makeAutoObservable, observable, action } from "mobx";
import { createContext, useContext } from "react";
import {ip} from '../requestCofing.js'
import { wss } from "../components/websocketConfig.js";

class SellerProfileStore {
    page = null
    products = null
    shop = null
    messages = null
    activeChat = null
    chatsList = null
    wss = null
    constructor (props) {
        makeAutoObservable(this)
        this.createWss(props);
    }
    setActiveChat = action((activeChat) => {
        this.activeChat = activeChat;
    })

    createProduct = action((e, name, product, description, image, cost, category) => {
        const formData = new FormData()
        formData.append('image', image)
        formData.append('idUser', this.shop.idUser)
        formData.append('idShop', this.shop.id)
        formData.append('product', product)
        formData.append('name', name)
        formData.append('cost', cost)
        formData.append('description', description)
        formData.append('category', category)
        e.preventDefault()
        fetch(`${ip}/shops/create-product`, {
            method: "POST",
            body: formData
        }).then(response => {
            if (response.status === 200) {
                response.json().then(result => {
                    this.products.push(result.product)
                    this.page = 'products'
                })
            }
        })
    })

    createWss = action(() => {
        const wss = new WebSocket('ws://89.23.100.86:8080/');
        wss.onmessage = (event) => {
            const message = JSON.parse(event.data)
            if (this.activeChat) {
                if (message.type == 'newMessage' && message.targetChatId === this.activeChat.id) {
                    this.getChats()
                    if (this.activeChat) this.getMessages(this.activeChat)
                }
            }
            
        }
        this.wss = wss
    })

    sendMessage = action((e, message, scrollToBottom) => {
        e.preventDefault()
        e.target.reset()
        this.messages.push({message: message, idUser: this.shop.idUser})
        scrollToBottom()
        fetch(`${ip}/shops/send-message`, {
            method: "POST",
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                idUser: this.shop.idUser,
                idChat: this.activeChat.id,
                message: message,
                idShop: this.shop.id,
            })
        }).then(response => {
            // response.json().then(result => {
            //     this.messages.push(result.message)
            // })
        })
    })

    getChats = action(() => {
        fetch(`${ip}/shops/get-chats`, {
            method: "GET",
            headers: {'Content-Type': 'application/json', 'idshop': this.shop.id}
        }).then(response => { 
            if (response.status === 200) {
                response.json().then(result => {
                    this.chatsList = result.chats
                })
            }
        })
    }) 

    getMessages = action((chat) => {
        this.activeChat = chat
        fetch(`${ip}/shops/get-messages`, {
            method: "GET",
            headers: {'Content-Type': 'application/json', 'idchat': this.activeChat.id}
        }).then(response => {
            if (response.status === 200) {
                response.json().then(result => {
                    this.messages = result.messages
                })
            }
        })
    })

    getShop = action((idUser) => {
        fetch(`${ip}/shops/get-shop`, {
            method: "GET",
            headers: {'Content-Type': 'application/json', 'idshop': idUser}
        }).then(response => {
            if (response.status === 200) {
                response.json().then(result => {
                    this.shop = result.shop
                })
            }
        })
    })

    getProducts = action(() => {
        fetch(`${ip}/shops/get-products`, {
            method: "GET",
            headers: {'Content-Type': 'application/json', 'idshop': this.shop.id}
        }).then(response => {
            if (response.status === 200) {
                response.json().then(result => {
                    this.products = result.products
                    console.log(result.products)
                })
            }
        })
    })
}


const sellerProfileStore = {
    sellerProfileStore: new SellerProfileStore(),
}

export const StoreContext = createContext(sellerProfileStore);

export const useSellerProfileStore = () => {
  return useContext(StoreContext);
};

export default SellerProfileStore;