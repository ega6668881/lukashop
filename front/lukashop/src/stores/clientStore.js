import { observable, action, makeAutoObservable } from "mobx";
import { createContext, useContext } from "react";
import {ip} from '../requestCofing.js'
import { send } from 'emailjs-com';

class ClientStore {
    user = {};
    page = null
    isLogin = false
    idUser = null
    balance = null
    userName = null
    emailVerif = null
    email = null
    emailVerifyCode = null
    nofication = null
    shop = false
    buyProducts = null
    constructor () {
        makeAutoObservable(this)
    }
    
    avatarUpload(e, setUser, file) {
        e.preventDefault()
        const formData = new FormData()
        formData.append('image', e.target.files[0])
        formData.append('id', this.idUser)
        fetch(`${ip}/users/avatar-upload`, {
            method: "POST",
            body: formData
        }).then(response => {
            response.json().then(result => {
                setUser(result.user)
            })
        })
    }

    getBuyProducts = action(() => {
        fetch(`${ip}/products/get-buy-products`, {
            method: "GET",
            headers: {'Content-Type': 'application/json', 'iduser': this.idUser}
        }).then(response => {
            if (response.status === 200) {
                response.json().then(result => {
                    console.log(result.buyProducts)
                    this.buyProducts = result.buyProducts
                })
            }
        })
    })

    sendNofication = action((content) => {
        this.nofication = content
        setTimeout(() => {
            this.nofication = null
        }, 6000)
    })

    setUser = action((user, shop) => {
        this.isLogin = true
        this.userName = user.login
        this.idUser = user.id
        this.emailVerif = user.emailVerif
        this.email = user.email
        this.shop = shop
        this.balance = user.balance
    })

    createShop = action((e, name, description, banner) => {
        console.log(e.target.banner.file)
        const formData = new FormData()
        formData.append('image', e.target.banner.files[0])
        formData.append('name', name)
        formData.append('description', description)
        formData.append('idUser', this.idUser)
        e.preventDefault()
        fetch(`${ip}/shops/create-shop`, {
            method: "POST",
            body: formData
        }).then(response => {
            if (response.status === 200) {
                response.json().then(result => {
                    this.shop = result.shop
                })
            }
        })
    })

    sendEmailCode = action((setEmailSend) => {
        setEmailSend(true)
        const verifCode = Math.floor(Math.random() * (9999))
        send(
            'service_qie90xn',
            'template_c7cbzrn',
            {to_name: this.userName, message: verifCode, email: this.email},
            'ihxfutZxUqTApgQ2c'
        ).then(response => {
            if (response.status === 200) {
                fetch(`${ip}/users/send-verify-email`, {
                    method: "POST",
                    headers: {'Content-Type': 'application/json'},
                    body: JSON.stringify({
                        idUser: this.idUser,
                        email: this.email,
                        verifyCode: verifCode,
        
                    })}).then(response => {
                        console.log(response.status)
                        if (response.status === 200) {
                            response.json().then(result => {
                                this.emailVerifyCode = result.verifyCode
                            })
                        }
                    })
            }
        })
        
    })

    getVerifyEmail = action(() => {
        fetch(`${ip}/users/get-verify-email`, {
            method: "GET",
            headers: {'Content-Type': 'application/json', 'iduser': this.idUser}
        }).then(response => {
            if (response.status === 200) {
                response.json().then(result => {
                    console.log(result)
                    this.emailVerifyCode = result.verification.verifyCode
                })
            } else if (response.status === 403) {
                this.emailVerifyCode = null
            }
        })
    })

    setVerifyEmail = action(() => {
        fetch(`${ip}/users/set-verify-email`, {
            method: "POST",
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                idUser: this.idUser,
                verifyEmail: true,
            })}).then(response => {
                if (response.status === 200) {
                    response.json().then(result => {
                        this.emailVerif = true
                    })
                }
                
            })
    })

    checkToken = action(() => {
        const token = localStorage.getItem("token")
        if (token) {
            fetch(`${ip}/auth/check-auth`, {
                method: "GET",
                headers: {'Content-Type': 'application/json', 'x-access-token': token}
            }).then(response => {
                if (response.status === 200) {
                    response.json().then(result => {
                        this.setUser(result.user, result.shop)
                    })
                } else return false
            })

        } else {
            return false
        }
    })
    registration = action((e, login, password, comparePassword, email, setErrorMessage) => {
        setErrorMessage("")
        e.preventDefault()
        if (password === comparePassword) {
            fetch(`${ip}/users/add-user`, {
                method: "POST",
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify({
                    login: login,
                    password: password,
                    email: email
                })}).then(response => {
                    switch(response.status) {
                        case 200: {
                            response.json().then(result => {
                                localStorage.setItem('token', result.token)
                                this.setUser(result.user)
                            })
                            break
                        }
                        case 403: {
                            setErrorMessage("Аккаунт с таким логином уже найден!")
                            break
                        }
                    }
                })

        } else {
            return setErrorMessage("Пароли не совпадают!")
        }
    })

    login = action((e, login, password, setErrorMessage) => {
        e.preventDefault()
        fetch(`${ip}/auth/login`, {
            method: "POST",
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                login: login,
                password: password
            })
        }).then(response => {
            switch(response.status) {
                case 200: {
                    response.json().then(result => {
                        localStorage.setItem('token', result.token)
                        this.setUser(result.user, result.shop)
                    })
                    break
                }
                case 401: {
                    setErrorMessage("Неверный пароль")
                    break
                }
                case 403: {
                    setErrorMessage("Неверный логин")
                    break
                }
                case 405: {
                    break
                }
            }
        })
    })
}


const clientStore =  {
    ClientStore: new ClientStore(),
}

export const StoreContext = createContext(clientStore);

export const useStore = () => {
  return useContext(StoreContext);
};

export default ClientStore;