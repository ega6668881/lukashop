import { makeAutoObservable, observable, action, autorun } from "mobx";
import { createContext, useContext } from "react";
import {ip} from '../requestCofing.js'
import ProductCard from "../components/productCard/card.jsx";


class BuyProductStore {
    product = null
    messages = []
    idChat = null
    wss = null;
    constructor () {
        makeAutoObservable(this)
        
    }
    createWss = action(() => {
        const wss = new WebSocket('ws://89.23.100.86:8080/')
        wss.onmessage = (event) => {
            const message = JSON.parse(event.data)
            if (message.type == 'newMessage' && message.targetChatId === this.idChat) {
                this.getMessages()
            }
        }
        this.wss = wss
    })

    init = (product) => {
        this.setProduct(product)
        this.setChatId(product.idChat)
        this.setMessages([])
        this.createWss()
        
    }

    setProduct = action((product) => {
        this.product = product;
    })

    setChatId = action((idChat) => {
        this.idChat = idChat;
    })

    setMessages = action((messages) => {
        this.messages = messages;
    })

    getMessages = action(() => {
        fetch(`${ip}/shops/get-messages`, {
            method: "GET",
            headers: {'Content-Type': 'application/json', 'idchat': this.idChat}
        }).then(response => {
            if (response.status === 200) {
                response.json().then(result => {
                    this.setMessages(result.messages)
                })
            }
        })
    })

    createChat = (async (idUser, e, message, scrollToBottom) => {
        const response = await fetch(`${ip}/shops/create-chat`, {
            method: "POST",
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                idUser: idUser,
                idShop: this.product.productInfo.idShop,
                idSeller: this.product.productInfo.idUser,
                idProduct: this.product.productInfo.id,
                idBuyProduct: this.product.id
            })
        })
        if (response.status === 200) {
            const result = await response.json()
            this.idChat = result.chat.id
            this.sendMessage(e, message, idUser, scrollToBottom)
        }
            
    })

    sendMessage = action(async (e, message, idUser, scrollToBottom) => {
        e.preventDefault()
        e.target.reset()
        if (this.idChat === null) {
            await this.createChat(idUser, e, message, scrollToBottom)
        } else {
            this.messages.push({message: message, idUser: idUser})
            scrollToBottom()
            fetch(`${ip}/shops/send-message`, {
                method: "POST",
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify({
                    idUser: idUser,
                    idChat: this.idChat,
                    message: message,
                    idShop: this.product.productInfo.idShop,
                })
            }).then(response => {
                // response.json().then(result => {
                //     this.messages.push(result.message)
                // })
            })
        }
        
        
    })
}


const buyProductStore = {BuyProductStore: new BuyProductStore()}


export const useBuyProductStore = (props) => {
  return useContext(createContext(buyProductStore));
};

export default BuyProductStore;