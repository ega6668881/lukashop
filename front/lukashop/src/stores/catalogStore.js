import { makeAutoObservable, observable, action, autorun } from "mobx";
import { createContext, useContext } from "react";
import {ip} from '../requestCofing.js'
import ProductCard from "../components/productCard/card.jsx";


class CatalogStore {
    category = null
    products = null
    constructor () {
        makeAutoObservable(this)
    }
    getProducts = action(() => {
        fetch(`${ip}/products/get-product`, {
            method: "GET",
            headers: {'Content-Type': 'application/json', 'category': this.category.toString()}
        }).then(response => {
            if (response.status == 200) {
                response.json().then(result => {
                    this.products = []
                    result.products.forEach(product => {
                        this.products.push(<ProductCard product={product}/>)
                    })
                })
                
            }
        })
    })
}


const searchStore =  {
    CatalogStore: new CatalogStore(),
}

export const StoreContext = createContext(searchStore);

export const useCatalogStore = () => {
  return useContext(StoreContext);
};

export default CatalogStore;