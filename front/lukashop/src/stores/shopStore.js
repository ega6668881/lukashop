import { makeAutoObservable, observable, action } from "mobx";
import { createContext, useContext } from "react";
import {ip} from '../requestCofing.js'

class ShopStore {
    shop = null
    popularProducts = null
    goodFeedBacks = []
    badFeedBacks = []
    constructor () {
        makeAutoObservable(this)
    }
    getShop = action((id) => {
        fetch(`${ip}/shops/get-shop`, {
            method: "GET",
            headers: {'Content-Type': 'application/json', 'idshop': id}
        }).then(response => {
            if (response.status == 200) {
                response.json().then(result => {
                    console.log(result)
                    this.shop = result.shop
                    this.popularProducts = result.popularProducts
                    if (result.goodFeedBacks) this.goodFeedBacks = result.goodFeedBacks
                    if (result.badFeedBacks) this.badFeedBacks = result.badFeedBacks
                })
            }
        })
    })
}


const shopStore =  {
    ShopStore: new ShopStore(),
}

export const StoreContext = createContext(shopStore);

export const useShopStore = () => {
  return useContext(StoreContext);
};

export default ShopStore;