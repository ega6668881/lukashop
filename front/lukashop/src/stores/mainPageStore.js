import { makeAutoObservable, observable, action } from "mobx";
import { createContext, useContext } from "react";
import {ip} from '../requestCofing.js'

class MainPageStore {
    marketingProducts = []
    constructor () {
        this.getMarketingProducts()
        makeAutoObservable(this)
    }
    getMarketingProducts = action(() => {
        fetch(`${ip}/products/get-marketing-products`, {
            method: "GET",
            headers: {'Content-Type': 'application/json'}
        }).then(response => {
            response.json().then(result => {
                console.log(result.rows)
                this.marketingProducts = result.rows
            })
        })
    })
}


const mainPageStore =  {
    MainPageStore: new MainPageStore(),
}

export const StoreContext = createContext(mainPageStore);

export const useMainPageStore = () => {
  return useContext(StoreContext);
};

export default MainPageStore;