import logo from './logo.svg';
import './App.css';
import Login from './components/auth/login/login'
import Header from './components/header/header'
import { useStore } from './stores/clientStore';
import MainPage from './components/mainPage/mainPage'
import { Routes, Route, Navigate } from 'react-router-dom';
import Profile from './components/profile/profile';
import AboutProduct from './components/productAbout/productAbout';
import Feedbacks from './components/productAbout/feedbacks/feedbacks';
import Registration from './components/auth/register/register';
import VerifyEmail from './components/auth/verifyEmail/verifyEmail';
import CheckAuth from './components/auth/checkAuth/checkAuth'
import Catalog from './components/catalog/catalog';
import ShopAbout from './components/shop/shopAbout/shopAbout';
import Search from './components/search/search';
import CreateShop from './components/shop/createShop/createShop';
import SellerProfile from './components/sellerProfile/sellerProfile';
import BuyProducts from './components/buyProducts/buyProducts';
import Donate from './components/donate/donate';
import HistoryPaynaments from './components/historyPaynaments/historyPaynaments';


function App() {
  const clientStore = useStore().ClientStore
  clientStore.checkToken()
  return (
    <div className="App">
      <Header />
      <Routes>
        <Route path="/" element={<MainPage />}/>
        <Route path="/auth/login" element={<Login />}/>
        <Route path="/auth/register" element={<Registration />}/>
        <Route path="/auth/verify-email" element={<CheckAuth Component={<VerifyEmail />}/>}/>
        <Route path="/shops/create-shop" element={<CheckAuth Component={<CreateShop />}/>}/>
        <Route path="/shops/:idShop" element={<ShopAbout />}/>
        <Route path="/profile/:id" element={<Profile />}/>
        <Route path="/shops/:idShop/products/:id" element={<AboutProduct />}/>
        <Route path="/products/feedbacks/:idProduct" element={<Feedbacks />}/>
        <Route path="/catalog/:category" element={<Catalog />}/>
        <Route path="/search/:searchItem" element={<Search />}/>
        <Route path="/seller-profile" element={<CheckAuth Component={<SellerProfile />}/>}/>
        <Route path="/buy-products" element={<CheckAuth Component={<BuyProducts />}/>}/>
        <Route path="/donate" element={<CheckAuth Component={<Donate />}/>}/>
        <Route path="/seller-profile/add-product" element={<CheckAuth Component={<Donate />}/>}/>
        <Route path="/history-paynament" element={<CheckAuth Component={<HistoryPaynaments />}/>}/>
      </Routes>
    </div>
  )
}

export default App;
