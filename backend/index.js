
// import bcrypt from 'bcrypt'
import {WebSocketServer} from 'ws'
import http from 'http'

import bodyParser from 'body-parser'
import productsRoutes from "./routes/products.js"
import usersRoutes from './routes/users.js'
import knexfile from './db/knexfile.cjs'
import authRoutes from './routes/auth.js'
import shopsRoutes from './routes/shops.js'
import paynaments from './routes/paynament.js'
import express from 'express'
import jwt from 'jsonwebtoken'
import dotenv from 'dotenv'
import Knex from 'knex'
import path from 'path'
import expressWs from 'express-ws'
import { API } from 'yoomoney-sdk'

import { getGlobals } from 'common-es'
const { __dirname, __filename } = getGlobals(import.meta.url)



dotenv.config()
const app = express()
const api = new API(process.env.YOOMONEY_TOKEN)
app.use(bodyParser.json())
const urlencodedParser = express.urlencoded({extended: false});
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.header('Access-Control-Allow-Headers', '*');
    next();
});
const server = http.createServer(app)

app.use('/api/v1/products', productsRoutes)
app.use('/api/v1/users', usersRoutes)
app.use('/api/v1/auth', authRoutes)
app.use('/api/v1/shops', shopsRoutes)
app.use('/api/v1/paynaments', paynaments)
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true, parameterLimit: 50000 }))
app.use('/uploads', express.static(path.join(__dirname, 'uploads')))
export const wss = new WebSocketServer({server})
server.listen(8080, () => {
    console.log(`Server started on port ${server.address().port}`);
}); 

app.listen(3000, () => {
  
})

export const knex = Knex(knexfile.development)

export default app
