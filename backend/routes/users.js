import bcrypt, { hashSync } from 'bcrypt'
import Knex from 'knex'
import jwt from 'jsonwebtoken'
import knexfile from '../db/knexfile.cjs'
import { errorCodes } from '../errorCodes.js'
import express from 'express'
import bodyParser from 'body-parser'
import multer from 'multer'
import fileMiddeware from '../middleware/file.js'

const router = express.Router();

const knex = Knex(knexfile.development)
const upload = multer({ dest: 'uploads/' });
const saltRounds = 10;
const salt = bcrypt.genSaltSync(saltRounds);
router.use(bodyParser.urlencoded({ limit: "50mb", extended: true, parameterLimit: 50000 }))
function generateAccessToken(username) {
    return jwt.sign(username, process.env.TOKEN_SECRET, { expiresIn: '1800s' });
}

router.post('/avatar-upload', fileMiddeware.single('image'), (req, res) => {
    const image = req.file
    if (!image) {
        res.status(400).json({success: false, error: "Фаил не найден"})
    } else {
        knex('users').update({avatar: req.file.path}).where({id: req.body.id})
        .returning("*")
        .then(rows => {
            res.status(200).json({success: true, message: "Успех!", user: rows[0]})
        })
        
    }
})

router.get('/get-user', (req, res) => {
    if ('iduser' in req.headers) {
        knex('users').select('*').where({id: req.headers['iduser']})
        .then(rows => {
            if (rows.length > 0) {
                knex('shops').select('*').where({idUser: req.headers['iduser']})
                .then(shops => {
                    let shop = false
                    if (shops.length > 0) {
                        shop = true
                    }
                    res.status(200).json({success: true, user: rows[0], shop: shop})
                })
                
            } else {
                res.status(403).json({success: true, error: "Пользователь не найлен"})
            }
            
        })
        .catch(err => {
            console.log(err)
            
        })

    } else {
        res.status(405).json({error: errorCodes['405']})
    }
})

router.post('/send-verify-email', (req, res) => {
    const checkBody = ['idUser', 'email', 'verifyCode']
    if (JSON.stringify(checkBody) === JSON.stringify(Object.keys(req.body))) {
        knex('email_verification').select("*").where({idUser: req.body.idUser})
        .then(rows => {
            if (rows == 0) {
                knex('email_verification').insert({idUser: req.body.idUser, createdAt: new Date,
                    updatedAt: new Date, verifyCode: req.body.verifyCode, email: req.body.email})
                .then(rows => {
                    res.status(200).json({success: true, verifCode: req.body.verifyCode})
                })
            } else {
                res.status(403).json({success: false, error: "Код уже был отправлен!"})
            }
        })
        
        
    } else {
        res.status(405).json({error: errorCodes['405']})
    }
})

router.get('/get-verify-email', (req, res) => {
    if ('iduser' in req.headers) {
        knex('email_verification').select('*').where({idUser: req.headers['iduser']})
        .then(rows => {
            if (rows.length == 0) {
                res.status(403).json({success: true, verification: []})
            } else {
                res.status(200).json({success: true, verification: rows[0]})
            }
            
        })
        
    } else {
        res.status(405).json({error: errorCodes['405']})
    }
})

router.post('/set-verify-email', (req, res) => {
    const checkBody = ['idUser', 'verifyEmail']
    if (JSON.stringify(checkBody) === JSON.stringify(Object.keys(req.body))) {
        knex('users').update({"emailVerif": req.body.verifyEmail}).where({id: req.body.idUser})
        .then(result => {
            knex('email_verification').delete().where({idUser: req.body.idUser})
            .then(result => {
                res.status(200).json({success: true, verifyEmail: req.body.verifyEmail})
            })
            
        })
        
    } else {
        res.status(405).json({error: errorCodes['405']})
    }
})

router.post('/add-user', (req, res) => {
    const checkBody = ['login', 'password', 'email']
    if (JSON.stringify(checkBody) === JSON.stringify(Object.keys(req.body))) {
        knex('users').select('*').where({'login': req.body.login})
        .then(rows => {
            if (rows.length > 0) {
                return res.status(403).json({success: false, error: "Пользователь уже существует"})

            } else {
                const hashPassword = hashSync(req.body.password, 10)
                knex('users').insert({login: req.body.login, password: hashPassword, emailVerif: false, 
                                    email: req.body.email, balance: 0, createdAt: new Date(), updatedAt: new Date()})
                .returning('*')
                .then(result => {
                    return res.status(200).json({success: true, token: generateAccessToken({login: req.body.login, id: result[0].id}),
                                    user: result[0]})
                })
            }
        })

    } else {
        res.status(405).json({error: errorCodes['405']})
    }
})

export default router