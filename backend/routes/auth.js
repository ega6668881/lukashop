import bcrypt from 'bcrypt'
import Knex from 'knex'
import jwt from 'jsonwebtoken'
import knexfile from '../db/knexfile.cjs'
import express from 'express'
import { errorCodes } from '../errorCodes.js'

const router = express.Router();

const knex = Knex(knexfile.development)

const comparePassword = (password, hashedPassword) =>
    bcrypt.compareSync(password, hashedPassword);

function generateAccessToken(username) {
    return jwt.sign(username, process.env.TOKEN_SECRET, { expiresIn: '1800s' });
}



router.get('/check-auth', (req, res) => {
    const token = req.headers['x-access-token'];
    if (!token) {
        res.status(405).json({success: false, error: errorCodes['405']});
    } else {
        jwt.verify(token, process.env.TOKEN_SECRET, function(err, decoded) {
            if (err) {
                return res.status(401).json({success: false, error: "Недействительный токен"})

            } else {
                knex('users').select("*").where({id: decoded.idUser})
                .then(rows => {
                    if (rows.length > 0) {
                        knex('shops').select('*').where({idUser: rows[0].id})
                        .then(shops => {
                            let shop = false
                            if (shops.length > 0) {
                                shop = true
                            }
                            return res.status(200).json({success: true, token: token, user: rows[0], shop: shop})
                        })
                        

                    } else {
                        return res.status(403).json({success: false, error: "Пользователь не найден!"})
                    }
                })
                .catch(err => {
                    return res.status(403).json({success: false, error: "Пользователь не найден!"})
                })
            }
        });
    }
})

router.post("/login", (req, res) => {
    const checkBody = ['login', 'password']
    if (JSON.stringify(checkBody) === JSON.stringify(Object.keys(req.body))) {
        
        knex('users').select('*').where({'login': req.body.login})
        .then(rows => {
            if (rows.length > 0) {
                const isValidPassowrd = comparePassword(req.body.password, rows[0].password)
                if (isValidPassowrd) {
                    knex('shops').select('*').where({idUser: rows[0].id})
                    .then(shops => {
                        let shop = false
                        if (shops.length > 0) {
                            shop = true
                        }
                        return res.status(200).json({success: true, token: generateAccessToken({login: req.body.login, idUser: rows[0].id}),
                        user: rows[0], shop: shop})
                    })
                    

                } else {
                    return res.status(401).json({success: false, error: "Неправильный пароль"})
                }

            } else {
                return res.status(403).json({success: false, error: "Пользователь не найден!"})
            }
        })
        .catch(err => {
            console.log(err)
        })

    } else {
        return res.status(405).json({success: false, error: errorCodes['405']})
    }
})

export default router