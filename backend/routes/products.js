import bcrypt from 'bcrypt'
import express from 'express'
import knexfile from '../db/knexfile.cjs'
import Knex from 'knex'
import { errorCodes } from '../errorCodes.js'

const router = express.Router();

const knex = Knex(knexfile.development)


router.get("/get-product", (req, res) => {
    if ("idproduct" in req.headers) {
        knex('products').select('*').where({'id': req.headers['idproduct']})
        .then(products => {
            knex('feedbaks').select('*').where({'idProduct': req.headers['idproduct']})
            .then(feedbacks => {
                knex('shops').select('*').where({id: products[0].idShop})
                .then(shop => {
                    res.status(200).json({success: true, product: products[0], feedbacks: feedbacks, shop: shop[0]})
                })
            })
            
        })
    } else if ("category" in req.headers) {
        knex('products').select('*').where({category: req.headers['category'], visible: true})
        .then(rows => {
            res.status(200).json({success: true, products: rows})
        })
    } else if ("search" in req.headers) {
        knex('products').select('*').where(knex.raw('lower(name)'), 'like', `%${req.headers['search'].toLowerCase()}%`).andWhere({visible: true})
        .then(rows => {
            res.status(200).json({success: true, result: rows})
        })

    } else {
        res.status(405).json({error: errorCodes['405']})
    }
    
})

router.get("/get-sells-leader-products", (req, res) => {

})

router.post("/add-product", (req, res) => {
    
})

router.get("/get-buy-products", (req, res) => {
    if ('iduser' in req.headers) {
        knex.transaction(async (trx) => {
            let buyProducts = await knex('buy_products').select("*").where({idUser: req.headers['iduser']})
            let counter = 0
            await Promise.all(buyProducts.map(async (buyProduct) => {
                const product = await knex('products').select("*").where({id: buyProduct.idProduct})
                buyProducts[counter]['productInfo'] = product[0]
                counter += 1
            }))
            res.status(200).json({success: true, buyProducts: buyProducts})
        })
    } else {
        res.status(405).json({success: false, error: errorCodes['405']})
    }
    
})

router.get("/get-marketing-products", (req, res) => {
    knex.transaction(async (trx) => { try { let firstFourRecords = await knex('marketing').select().where({publish: false}).limit(4);
    if (firstFourRecords.length == 0) {
        firstFourRecords = await knex('marketing').update({publish: false}).returning("*").limit(4)
        
    } 
    const relatedRecords = await Promise.all(firstFourRecords.map(async (record) => {
        const relatedRecord = await knex('products').where('id', record.idProduct).first();
        return relatedRecord;
      }));
        if (firstFourRecords.length) {
            if (firstFourRecords[0].publish_count - 1 == 0) {
                await knex('marketing').delete().where({id: firstFourRecords[0].id})
            } else {
                await knex('marketing').update({ publish: true, publish_count: firstFourRecords[0].publish_count-1 }).where('id', firstFourRecords[0].id);
            }
        }
        
    if (relatedRecords.length > 0) {
        res.status(200).json({success: true, rows: relatedRecords})
    } else {
        res.status(200).json({success: true, rows: []})
    }
    
    } catch (err) { console.error(err); res.status(404) } }) .then(() => {  });
    
})


export default router