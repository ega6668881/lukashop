import bcrypt from 'bcrypt'
import Knex from 'knex'
import jwt from 'jsonwebtoken'
import knexfile from '../db/knexfile.cjs'
import express from 'express'
import bodyParser from 'body-parser'
import expressWs from 'express-ws'
import { errorCodes } from '../errorCodes.js'
import {wss} from '../index.js'

const router = express.Router()
const knex = Knex(knexfile.development)

router.use(bodyParser.json());
router.use(express.urlencoded({ extended: true }));

router.get("/get-paynaments", (req, res) => {
    if ("iduser" in req.headers) {
        knex('paynaments').select("*").where({idUser: req.headers['iduser'], live: false}).orderBy('createdAt', 'asc')
        .then(paynaments => {
            res.status(200).json({success: true, paynaments: paynaments})
        })

    } else {
        res.status(405).json({error: errorCodes['405']})
    }
})

router.post("/create-paynament", (req, res) => {
    if ("idUser" in req.body && "label" in req.body && "amount" in req.body) {
        knex.transaction(async (trx) => {
            const paynaments = await knex('paynaments').select("*").where({idUser: req.body.idUser})
            if (paynaments.length > 0) {
                await knex('paynaments').delete().where({id: paynaments[0].id})
            }
            await knex('paynaments').insert({live: true, idUser: req.body.idUser, label: req.body.label, amount: req.body.amount,
                createdAt: new Date(), updatedAt: new Date})
            
            res.status(200).json({success: true})
        })
        
    } else {
        res.status(405).json({error: errorCodes['405']})
    }
    
}) 

router.post("/nofications", (req, res) => {
    knex('paynaments').select("*").where({idUser: req.body.label, live: true})
    .then(paynaments => {
        if (paynaments.length > 0) {
            knex('paynaments').update({live: false}).where({id: paynaments[0].id})
            .then(result => {
                knex('users').increment('balance', paynaments[0].amount).where({id: req.body.label})
                .then(result => {
                    res.status(200).json({success: true})
                })
            })
 
        } else {
            res.status(403).json({success: true, error: "Платеж не найден"})
        }
    })
})


export default router