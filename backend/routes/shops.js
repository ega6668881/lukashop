import bcrypt from 'bcrypt'
import Knex from 'knex'
import jwt from 'jsonwebtoken'
import knexfile from '../db/knexfile.cjs'
import bodyParser from 'body-parser'
import express from 'express'
import expressWs from 'express-ws'
import { errorCodes } from '../errorCodes.js'
import {wss} from '../index.js'
import fileMiddeware from '../middleware/file.js'
import multer from 'multer'

const router = express.Router()
const knex = Knex(knexfile.development)
const upload = multer({ dest: 'uploads/' });
router.use(bodyParser.urlencoded({ limit: "50mb", extended: true, parameterLimit: 50000 }))

router.get("/get-shop", (req, res) => {
    if ('idshop' in req.headers) {
        knex('shops').select("*").where(function() {
            this.where({id: req.headers['idshop']}).orWhere({idUser: req.headers['idshop']})
        })
        .then(shops => {
            if (shops.length == 0) {
                res.status(401).json({success: true, error: "Магазин не найден"})
            } else {
                knex('products').select("*").where({idShop: shops[0].id}).orderBy('sellCount', 'desc').limit(3)
                .then(products => {
                    knex('feedbaks').select("*").where({score: false, idShop: shops[0].id})
                    .then(badFeedbacks => {
                        knex('feedbaks').select("*").where({score: true, idShop: shops[0].id})
                        .then(goodFeedBacks => {
                            res.status(200).json({success: true, shop: shops[0], popularProducts: products,
                                badFeedbacks: badFeedbacks, goodFeedBacks: goodFeedBacks})
                        })
                    })
                    
                })
                
            }
        })

    } else {
        res.status(405).json({success: false, error: errorCodes['405']})
    }
})

router.get('/get-messages', (req, res) => {
    if ('idchat' in req.headers) {
        knex('messages').select("*").where({idChat: req.headers['idchat']})
        .then(messages => {
            res.status(200).json({success: true, messages: messages})
        })

    } else {
        res.status(405).json({success: false, error: errorCodes['405']})
    }
})

router.post("/create-chat", (req, res) => {
    const checkBody = ['idUser', 'idShop', 'idSeller', 'idProduct', 'idBuyProduct']
    if (JSON.stringify(checkBody) === JSON.stringify(Object.keys(req.body))) {
        knex('chats').insert({idUser: req.body.idUser, idSeller: req.body.idSeller,
            idShop: req.body.idShop, idProduct: req.body.idProduct, createdAt: new Date, updatedAt: new Date()})
        .returning("*")
        .then(chats => {
            knex('buy_products').update({idChat: chats[0].id}).where({id: req.body.idBuyProduct})
            .then(resut => {
                res.status(200).json({success: true, chat: chats[0]})
            })
            .catch(error => {
                console.log(error)
            })
            
        })

    } else {
        res.status(405).json({success: false, error: errorCodes['405']})
    }
})

router.post("/send-message", (req, res) => {
    const checkBody = ['idUser', 'idChat', 'message', 'idShop']
    if (JSON.stringify(checkBody) === JSON.stringify(Object.keys(req.body))) {
        knex('messages').insert({idUser: req.body.idUser, message: req.body.message, idChat: req.body.idChat, createdAt: new Date(), updatedAt: new Date()})
        .returning("*")
        .then(result => {
            knex('chats').update({updatedAt: new Date()}).where({id: req.body.idChat})
            .then(result => {
                wss.clients.forEach(client => {
                    client.send(JSON.stringify({type: "newMessage", targetChatId: req.body.idChat}))
                    
                })
                return res.status(200).json({success: true, message: result[0]})
            })
        })

    } else {
        res.status(405).json({success: false, error: errorCodes['405']})
    }
})

router.post("/create-product", fileMiddeware.single('image'), (req, res) => {
    if (req.body) {
        try {
            const productCount = req.body.product.split('~').length
            knex('products').insert({idUser: req.body.idUser, idShop: req.body.idShop, image: req.file.path,
                product: req.body.product, productCount: productCount, profit: 0, sellCount: 0, createdAt: new Date(),
                updatedAt: new Date(), visible: true, description: req.body.description, category: req.body.category,
                name: req.body.name, cost: req.body.cost, score: 0})
            .returning("*")
            .then(products => {
                res.status(200).json({success: true, product: products[0]})
            })

        } catch(error) {
            console.log(error)
            res.status(405).json({success: false, error: errorCodes['405']})
        }
    }

})

router.post("/buy-product", (req, res) => {
    const checkBody = ['idUser', 'idProduct']
    if (JSON.stringify(checkBody) === JSON.stringify(Object.keys(req.body))) {
        knex.transaction(async (trx) => {
            const product = await knex('products').select("*").where({id: req.body.idProduct}).first()
            const user = await knex('users').select("*").where({id: req.body.idUser}).first()
            if (user.balace < product.cost) {
                return res.status(403).json({success: false, error: "У пользователя недостаточно баланса для покупки"})
            } else {
                if (product.productCount <= 0) {
                    res.status(401).json({success: true, error: "Товар закончился"})
                } else {
                    let productRewards = product.product.split('~')
                    let buyProductReward = productRewards.shift()
                    let visible = true
                    if (productRewards.length == 0) {
                        visible = false
                    }
                    await knex('users').increment('balance', product.cost).where({id: product.idUser})
                    await knex('users').increment('balance', -product.cost).where({id: req.body.idUser})
                    await knex('products').update({product: product.product.slice(buyProductReward.length + 1),
                        productCount: productRewards.length - 1, sellCount: product.sellCount + 1, visible: visible, profit: product.profit + product.cost}).where({id: product.id})
                    const buyProducts = await knex('buy_products').insert({idProduct: product.id, idUser: req.body.idUser, idChat: null,
                        product: buyProductReward, cost: product.cost, createdAt: new Date(), updatedAt: new Date()}).returning("*")
                    res.status(200).json({success: true, buyProduct: buyProducts})
                }
                
            }
            
        })

    } else {
        res.status(405).json({success: false, error: errorCodes['405']})
    }
})

router.post("/create-shop", fileMiddeware.single('image'), (req, res) => {
    const checkBody = ['name', 'description', 'image', 'idUser']
    if (req.body) {
        try {
            knex('shops').select("*").where({idUser: req.body.idUser})
            .then(shops => {
                if (shops.length === 0) {
                    knex('shops').insert({name: req.body.name, banner: req.file.path,
                        description: req.body.description, idUser: req.body.idUser, profit: 0, sellsCount: 0, score: 0,
                        createdAt: new Date(), updatedAt: new Date()})
                    .returning("*")
                    .then(shops => {
                        res.status(200).json({success: true, shop: shops[0]})
                    })

                } else {
                    res.status(401).then({success: false, error: "Магазин уже существует"})
                }
            })
        } catch (error) {
            console.log(error)
        }
        

    } else {
        res.status(405).json({success: false, error: errorCodes['405']})
    }
})

router.get("/get-chats", (req, res) => {
    if ('idshop' in req.headers) {
        knex.transaction(async (trx) => {
            let chats = await knex('chats').select("*").where({idShop: req.headers['idshop']}).orderBy('updatedAt', 'asc')
            let counter = 0
            await Promise.all(chats.map(async (chat) => {
                const user = await knex('users').select("*").where({id: chat.idUser})
                const lastMessage = await knex('messages').select("*").where({idChat: chat.id}).orderBy('createdAt', 'desc')
                const product = await knex('products').select("*").where({id: chat.idProduct})
                chats[counter]['user'] = user[0]
                chats[counter]['lastMessage'] = lastMessage[0]
                chats[counter]['product'] = product[0]
                counter += 1
            }))
            res.status(200).json({success: true, chats: chats})
        })
        
        
    } else {
        res.status(405).json({success: false, error: errorCodes['405']})
    }
})

router.get("/get-products", (req, res) => {
    if ('idshop' in req.headers) {
        knex.transaction(async (trx) => {
            try {
                const products = await knex('products').select("*").where({idShop: req.headers['idshop']})
                let newProducts = []
                let counter = 0
                const feedbacks = await Promise.all(products.map(async (product) => {
                    const relatedRecord = await knex('feedbaks').where('idProduct', product.id);
                    products[counter]['feedBacks'] = relatedRecord
                    counter += 1
                  }));
                res.status(200).json({success: true, products: products})

            } catch (error) {
                console.log(error)
            } 
        })
        
    } else {
        res.status(405).json({success: false, error: errorCodes['405']})
    }
})

export default router