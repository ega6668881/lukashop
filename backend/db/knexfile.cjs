// Update with your config settings.

/**
 * @type { Object.<string, import("knex").Knex.Config> }
 */
module.exports = {

  development: {
    client: 'pg',
    connection: {
      host: "127.0.0.1",
      user: 'postgres',
      password: '3251',
      port: 5432,
      database: 'lukashop_dev',
      
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      directory: "./migrations"
    }
  },
};
