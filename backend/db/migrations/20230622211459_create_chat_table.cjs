exports.up = function(knex) {
    return knex.schema.createTable('messages', function(table) {
        table.increments('id').primary()
        table.integer('idUser').unsigned()
        table.foreign('idUser').references('users.id').onDelete('cascade');
        table.integer('idChat').unsigned()
        table.foreign('idChat').references('chats.id').onDelete('cascade');
        table.text('message').notNullable()
        table.timestamp('createdAt').notNullable()
        table.timestamp('updatedAt').notNullable()
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('messages')
};
