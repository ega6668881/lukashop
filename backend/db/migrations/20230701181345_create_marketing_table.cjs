
exports.up = function(knex) {
    return knex.schema.createTable('marketing', function(table) {
        table.increments('id').primary()
        table.integer('idProduct').unsigned()
        table.foreign('idProduct').references('products.id').onDelete('cascade');
        table.boolean('publish')
        table.integer('publish_count')
        table.timestamp('createdAt').notNullable()
        table.timestamp('updatedAt').notNullable()
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('marketing')
};
