exports.up = function(knex) {
    return knex.schema.createTable('email_verification', function(table) {
        table.increments('id').primary()
        table.integer('idUser').unsigned()
        table.foreign('idUser').references('users.id').onDelete('cascade');
        table.string('email').notNullable()
        table.string('verifyCode').notNullable()
        table.timestamp('createdAt').notNullable()
        table.timestamp('updatedAt').notNullable()
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('email_verification')
};
