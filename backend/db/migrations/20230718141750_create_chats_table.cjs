
exports.up = function(knex) {
    return knex.schema.createTable('chats', function(table) {
        table.increments('id').primary()
        table.integer('idUser').notNullable()
        table.integer('idSeller').notNullable()
        table.integer('idProduct').unsigned()
        table.foreign('idProduct').references('products.id').onDelete('cascade');
        table.integer('idShop').unsigned()
        table.foreign('idShop').references('shops.id').onDelete('cascade');
        table.timestamp('createdAt').notNullable()
        table.timestamp('updatedAt').notNullable()
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('chats')
};
