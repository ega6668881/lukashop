
exports.up = function(knex) {
    return knex.schema.createTable('paynaments', function(table) {
        table.increments('id').primary()
        table.integer('idUser').unsigned()
        table.foreign('idUser').references('users.id').onDelete('cascade');
        table.string('label')
        table.integer('amount')
        table.boolean('live').notNullable()
        table.timestamp('createdAt').notNullable()
        table.timestamp('updatedAt').notNullable()
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('paynaments')
};
