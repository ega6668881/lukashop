
exports.up = function(knex) {
    return knex.schema.createTable('shops', function(table) {
        table.increments('id').primary()
        table.integer('idUser').unsigned()
        table.string('banner')
        table.string('name')
        table.string('description')
        table.foreign('idUser').references('users.id').onDelete('cascade');
        table.float('profit')
        table.integer('sellsCount')
        table.integer('score')
        table.timestamp('createdAt').notNullable()
        table.timestamp('updatedAt').notNullable()
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('shops')
};
