exports.up = function(knex) {
    return knex.schema.createTable('buy_products', function(table) {
        table.increments('id').primary()
        table.integer('idUser').unsigned()
        table.foreign('idUser').references('users.id').onDelete('cascade');
        table.integer('idChat').unsigned()
        table.foreign('idChat').references('chats.id').onDelete('cascade');
        table.integer('idProduct').unsigned()
        table.foreign('idProduct').references('products.id').onDelete('cascade');
        table.string('product').notNullable()
        table.float('cost').notNullable()
        table.timestamp('createdAt').notNullable()
        table.timestamp('updatedAt').notNullable()
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('buy_products')
};
