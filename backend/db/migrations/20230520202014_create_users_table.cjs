
exports.up = function(knex) {
    return knex.schema.createTable('users', function(table) {
        table.increments('id').primary()
        table.string("login").notNullable()
        table.string("password").notNullable()
        table.string("email").notNullable()
        table.string("avatar")
        table.boolean("emailVerif").notNullable()
        table.integer("balance").notNullable()
        table.timestamp('createdAt').notNullable()
        table.timestamp('updatedAt').notNullable()
    })
};

exports.down = function(knex) {
  return knex.schema.dropTable('users')
};
