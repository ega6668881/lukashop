exports.up = function(knex) {
    return knex.schema.createTable('feedbaks', function(table) {
        table.increments('id').primary()
        table.integer('idUser').unsigned()
        table.foreign('idUser').references('users.id').onDelete('cascade');
        table.string('message').notNullable()
        table.boolean('score')
        table.integer('idProduct').unsigned()
        table.foreign('idProduct').references('products.id').onDelete('cascade');
        table.integer('idShop').unsigned()
        table.foreign('idShop').references('shops.id').onDelete('cascade');
        table.timestamp('createdAt').notNullable()
        table.timestamp('updatedAt').notNullable()
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('feedbaks')
};
