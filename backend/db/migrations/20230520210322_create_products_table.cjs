
exports.up = function(knex) {
    return knex.schema.createTable('products', function(table) {
        table.increments('id').primary()
        table.integer('idUser').unsigned()
        table.foreign('idUser').references('users.id').onDelete('cascade');
        table.float('cost').notNullable()
        table.integer('sellCount').notNullable()
        table.integer('profit').notNullable()
        table.string('image')
        table.integer('score')
        table.string('name').notNullable()
        table.text('description').notNullable()
        table.text('product').notNullable()
        table.integer('productCount')
        table.integer('idShop').unsigned()
        table.foreign('idShop').references('shops.id').onDelete('cascade');
        table.string('category').notNullable()
        table.boolean('visible')
        table.timestamp('createdAt').notNullable()
        table.timestamp('updatedAt').notNullable()
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('products')
};
